﻿#!python3
# coding: utf-8

import os
import sys
import textwrap
import requests

from lxml import html, etree
from urllib.parse import urlparse


class ArticleExtractor:

    def __init__(self, url):
        self.url = url

    def extract(self):
        """ Download and parse html page containing the article."""
        response = requests.get(self.url)
        tree = html.fromstring(response.text)

        """ Our content will be inside of body, of course."""
        body_el = tree.find('body')

        """ Recursively score elements."""
        self._score_element(body_el)

        """ Take top ten rated elements."""
        elements_by_cnr = sorted(body_el.iterdescendants(), 
                                 key=lambda el: float(el.attrib['cnr']))
        top_ten = elements_by_cnr[-11:]

        """ Group top ten elements by their parent elements."""
        by_parent = dict()
        for el in top_ten:
            parent = el.getparent()
            if not parent in by_parent:
                by_parent[parent] = []
            by_parent[parent].append(el)

        main_candidate = sorted(by_parent.items(),
                                key=lambda x: len(x[1]))[-1][0]
        """ Process links."""
        links = main_candidate.findall('*//a')
        for link in links:
            link_text = link.text_content().strip()
            if not link_text:
                parent = link.getparent()
                parent.remove(link)
            else:
                link.text = '{0} [{1}]'.format(link_text, link.attrib['href'])
                for child in link:
                    link.remove(child)

        text_content = str()
        """ Construct article content."""
        wrapper = textwrap.TextWrapper(width=80, break_long_words=False)
        for child in main_candidate:
            child_text = child.text_content().strip()
            if not child_text:
                continue
            child_text_wrapped = wrapper.fill(child_text)
            if child.tag in ('p', 'div'):
                child_text_wrapped = '\n{0}\n'.format(child_text_wrapped)
            text_content += child_text_wrapped

        """ Remove leading and trailing whitespaces."""
        text_content = text_content.strip()
        return Article(text_content, self.url)


    def _score_element(self, el):
        weight = 1
        text_length = 0
        element_text = (el.text or '').strip()

        """ Find if element has children nodes."""
        children = list(el)
        if not children:
            if element_text:
                """ Element has text content."""
                text_length = len(element_text)
                el.set('cnr', str(len(element_text)))
            else:
                """ Neither children nor text in element."""
                el.set('cnr', '0')
        else:
            """ Element has children, rate them recurively."""
            for child in children:
                """ Remove comments and script tags."""
                if child.tag in (etree.Comment, 'script', 'img', 'style'):
                    el.remove(child)
                    continue

                child_weight, child_text_length = self._score_element(child)
                text_length += child_text_length
                weight += child_weight

            if element_text:
                text_length += len(element_text)

            el.set('cnr', str(text_length / weight))

        return weight, text_length


class Article:

    def __init__(self, content, url):
        self.content = content
        self.url = url

    def save(self):
        article_path = self._get_file_path()
        dirname = os.path.dirname(article_path)
        
        if not os.path.isdir(dirname):
            os.makedirs(dirname)

        with open(article_path, 'w', encoding='utf-8') as f:
            f.write(self.content)

    def _get_file_path(self):
        parsed_url = urlparse(self.url)
        url_path = parsed_url.path

        url_path = url_path.strip('/')

        file_path_chunks = url_path.split('/')
        file_path_chunks.insert(0, parsed_url.netloc)
        file_path_chunks[-1] = file_path_chunks[-1].split('.')[0] + '.txt'

        return os.sep.join(file_path_chunks)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: "python extractor.py <url>"')
        sys.exit(1)

    url = sys.argv[1]

    extractor = ArticleExtractor(url)
    article = extractor.extract()
    article.save()
